package main

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"flag"
	"fmt"
	"go/ast"
	"go/build"
	"go/importer"
	"go/parser"
	"go/token"
	"go/types"
	"hash"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const generatedFile = "worker_names.go"

// tooqs-namer generates topic and channel names for workers based on the name
// of the type implementing the workers.Handler interface, the method (Topic,
// Channel), and the contents of the package's implementation at time of
// execution. It generates code inside the package's directory to add `Topic()
// string` and `Channel() string` methods to a struct implementing the
// workers.Handler interface.
//
// This gives us a unique, deterministic, and versioned name for topics and
// channels for any given worker.
//
// Use this if you want to don't want to assign an explicit topic/channel name.
// It is not recommended to rely on the random name generation, but instead to
// use tooqs-namer so that the workers re-attach to the same topics and channels
// on restart.
//
// tooqs-namer requires the name of the target type on which to generate the
// Topic and Channel methods. We cannot rely on checking for a type that
// implements the Handler interface because tooqs-namer may run before the
// author implements the required methods.
//
// tooqs-namer can be run from the command-line with a path to the package
// directory, but also supports `go generate` by adding the following in the
// file with the worker implementation:
//
//		//go:generate tooqs-namer -type SomeType .
func main() {
	flag.Usage = Usage

	var tgtType = flag.String("type", "", "type name implementing workers.Handler")

	flag.Parse()
	args := flag.Args()

	if *tgtType == "" {
		fmt.Println("Missing type argument")
		flag.Usage()
		os.Exit(2)
	}

	if len(args) == 0 {
		// TODO parse files or dirs
		flag.Usage()
		os.Exit(2)
	}

	// TODO Rewrite this for potentially many files, directories, and packages.
	// This would be the start of iteration for one file in one package.
	dir := filepath.Dir(args[0])
	pkg, err := build.Default.ImportDir(dir, 0)
	if err != nil {
		log.Fatalf("pkg build error: %s", err)
	}

	var buf bytes.Buffer
	var s *bufio.Scanner

	// A package potentially contains many files, and many types and functions
	// defined in those files. Our goal is to identify some things for the files
	// given to us:
	//
	// - The package name that implements a Worker
	// - A hash of the contents of that package
	// - The name of the struct that implements the worker
	//
	// First we begin by walking through each file to build package contents as we
	// go as well as ensuring the package actually defines the target struct.
	structInPkg := false
	for _, gf := range pkg.GoFiles {
		// Skip generated files
		if gf == generatedFile {
			continue
		}

		fs := token.NewFileSet()
		path := filepath.Join(dir, gf)
		f, err := os.Open(path)
		if err != nil {
			log.Fatalf("file read error: %s", path)
		}

		// Add file contents to eventual package hash
		s = bufio.NewScanner(f)
		for s.Scan() {
			b := s.Bytes()
			if len(b) > 0 {
				buf.Write(b)
			}
		}
		f.Close()

		parsed, err := parser.ParseFile(fs, path, nil, 0)
		if err != nil {
			log.Fatalf("parsing package: %s: %s", path, err)
		}

		conf := types.Config{Importer: importer.Default()}
		chk, err := conf.Check(pkg.Name, fs, []*ast.File{parsed}, nil)
		if err != nil {
			log.Fatalf("checking package: %s: %s", f, err)
		}

		walkStructs(chk.Scope(), func(st *types.Struct, obj types.Object) bool {
			if extractPkg(obj.Name()) == *tgtType {
				structInPkg = true
				return false
			}
			return true
		})
	}

	if !(structInPkg && pkg.Name != "") {
		log.Fatalf("Unable to find package name and struct name in file set")
	}

	// Use the contents, method, and package name to generate a deterministic hash
	// value for each method return value.
	h := md5.New()
	io.Copy(h, &buf)
	h.Write([]byte(pkg.Name))
	h.Write([]byte("topic"))

	namer := generateNamer(pkg.Name, *tgtType, h)

	dp := filepath.Join(pkg.Dir, generatedFile)
	df, err := os.Create(dp)
	if err != nil {
		log.Fatalf("unable to open file to generate namer: %s", err)
	}

	io.Copy(df, bytes.NewReader(namer))
	log.Printf("Namer methods written to (%s) %s", pkg.Name, dp)
}

func Usage() {
	fmt.Fprintln(os.Stderr, "tooqs-namer -type <Handler Type> <filename>")
	fmt.Fprintln(os.Stderr, "tooqs-namer -type <Handler Type> <directory>")
}

func generateNamer(pkg, worker string, contents hash.Hash) []byte {
	var buf bytes.Buffer

	fmt.Fprintf(&buf, "package %s\n", pkg)
	fmt.Fprintln(&buf)
	fmt.Fprintln(&buf, "// Contents generated by tooqs-namer")
	generateMethod(&buf, worker, "Topic", contents)
	generateMethod(&buf, worker, "Channel", contents)

	return buf.Bytes()
}

func generateMethod(buf *bytes.Buffer, worker, method string, contents hash.Hash) {
	contents.Write([]byte(method))

	fmt.Fprintln(buf)
	fmt.Fprintf(buf, "func (%s) %s() string {\n", worker, method)
	fmt.Fprintf(buf, "\treturn \"%x\"\n", contents.Sum(nil))
	fmt.Fprintln(buf, "}")
}

// We can't always predict the resolved package pathname since it might be
// vendored into the consuming package. Therefore, we pop off the last value of
// the path and that should give us the worker package
func extractPkg(path string) string {
	if path == "" {
		return path
	}

	parts := strings.Split(path, "/")
	return parts[len(parts)-1]
}

// walkStructs iterates and filters all Struct objects in the given scope and
// calls the iterator with each struct found.
// The iterator can indicate the loop can proceed by returning true; returning
// false breaks the loop
func walkStructs(scope *types.Scope, fn func(*types.Struct, types.Object) bool) {
	cont := true

	for _, n := range scope.Names() {
		if !cont {
			break
		}

		obj := scope.Lookup(n)
		if st, ok := obj.Type().Underlying().(*types.Struct); ok {
			cont = fn(st, obj)
		}
	}
}
