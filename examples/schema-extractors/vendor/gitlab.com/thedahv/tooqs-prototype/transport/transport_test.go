package transport

import "testing"

func TestRandomName(t *testing.T) {
	names := make(map[string]struct{})
	for i := 0; i < 1000; i++ {
		n := randomName(20)
		if _, ok := names[n]; ok {
			t.Errorf("Got duplicate name %s after %d iterations\n", n, i)
			t.FailNow()
		}
	}
}
