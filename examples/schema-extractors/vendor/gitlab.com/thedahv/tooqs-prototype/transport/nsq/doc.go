// package nsq implements the Transporter interface for [NSQ](http://nsq.io/).
// NSQ is a "realtime distributed messaging platform" that supports distributed,
// scalable, and fault-tolerant work patterns. Its built-in behavior for error
// handling, retries, and queue volume make it an attractive choice for building
// scalable and resilient pipelines.
package nsq
