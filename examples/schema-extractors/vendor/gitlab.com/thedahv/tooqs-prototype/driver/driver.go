package driver

import (
	"log"

	"gitlab.com/thedahv/tooqs-prototype/transport"
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// Driver orchestrates the components that are composed together to register and
// connect a worker to its message source.
type Driver struct {
	transports []transport.Transporter
}

// Register connects handlers to a given transport and starts their listeners.
//
// TODO Add tracing/logging we can send to all transport/workers. Should
// implement the log interface. Should be overloaded so that any number of
// logging strategies could be configured
//
// Check out: https://www.youtube.com/watch?v=lzlGXMnrBgw at 25 minutes in to
// see how this could be done with channels
func (d *Driver) Register(t transport.Transporter, handlers ...workers.Handler) error {
	if err := t.Connect(); err != nil {
		return err
	}
	for _, h := range handlers {
		// TODO Handle restarts? Pass a channel in that we can select on so our
		// transports can communicate about transpot connection changes
		// Check out https://godoc.org/gopkg.in/tomb.v2

		// The given handler is also a worker if it implements the Publish
		// interface. Therefore, we check for that capability and add it as a
		// read/write worker if the worker needs to publish message. Otherwise, we
		// can add it as a read-only worker.
		//
		// This all runs at startup, and we don't want to go quietly into the night
		// if one of the workers fails to add. So we'll bail out at the first sign
		// of trouble and let the client know.
		if err := t.AddHandler(h); err != nil {
			return err
		}
	}
	d.transports = append(d.transports, t)

	return nil
}

// StopAll disconnects all workers and cleans up any resources they may be
// holding.
func (d *Driver) StopAll() {
	log.SetPrefix("[Driver/StopAll] ")
	log.Println("Tearing down transports")
	for _, t := range d.transports {
		t.Stop()
	}
	log.Println("Done")
}
