package compress

import (
	"bytes"
	"fmt"
	"testing"
)

func TestCompressionRoundtrip(t *testing.T) {
	pl := []byte(`<html>
	<body>
		<h1>hello world</h1>
		<script type='application/json+ld'>
			{
				"@context": "http://json-ld.org/contexts/person.jsonld",
				"@id": "http://dbpedia.org/resource/John_Lennon",
				"name": "John Lennon",
				"born": "1940-10-09",
				"spouse": "http://dbpedia.org/resource/Cynthia_Lennon"
			}
		</script>
	</body>
</html>
}`)

	// Write
	compressed, err := Compress(bytes.NewReader(pl))
	if err != nil {
		t.Errorf("error compressing payload: %v\n", err)
		t.FailNow()
	}

	fmt.Println(compressed)

	// Read
	decompressed, err := Decompress(bytes.NewReader(compressed))
	if err != nil {
		t.Errorf("error decompressing payload: %v\n", err)
		t.FailNow()
	}

	if bytes.Compare(decompressed, pl) != 0 {
		t.Errorf("Decompressed data does not match input\n")
		t.Errorf(string(decompressed))
	}
}
