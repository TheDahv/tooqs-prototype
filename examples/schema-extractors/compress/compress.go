package compress

import (
	"bytes"
	"compress/gzip"
	"io"
	"io/ioutil"
)

// Compress compacts a payload for sending between messages
func Compress(payload io.Reader) ([]byte, error) {
	var buf bytes.Buffer
	gz := gzip.NewWriter(&buf)

	if _, err := io.Copy(gz, payload); err != nil {
		return nil, err
	}

	if err := gz.Flush(); err != nil {
		return nil, err
	}

	gz.Close()

	return buf.Bytes(), nil
}

// Decompress unpacks a payload from a message
func Decompress(payload io.Reader) ([]byte, error) {
	gz, err := gzip.NewReader(payload)
	if err != nil {
		return nil, err
	}
	defer gz.Close()

	return ioutil.ReadAll(gz)
}
