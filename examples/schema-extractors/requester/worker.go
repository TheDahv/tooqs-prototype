package requester

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"strings"
	"time"

	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/compress"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/db"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/types"
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// SiteRequester implements a worker to handle requests to scrape a site for
// schema.org data and kicks off the pipeline to process its contents.
type SiteRequester struct {
	db  *db.Client
	bus *workers.MessageBus
}

// New creates a new SiteRequester worker with the configured db client
func New(db *db.Client) *SiteRequester {
	return &SiteRequester{db: db}
}

// Accepts defines the type of message SiteRequester can process
func (SiteRequester) Accepts() reflect.Type {
	return reflect.TypeOf(types.SiteRequest{})
}

// Topic creates a named topic for the beginning of the pipeline
func (SiteRequester) Topic() string {
	return "site-requested"
}

// Channel creates a named channel for the beginning of the pipeline
func (SiteRequester) Channel() string {
	return "start-pipeline"
}

// Concurrency sets how many messages Requester should process at once. This
// operation runs particularly fast, but we don't want to overwhelm downstream
// pipelines that run more slowly.
func (SiteRequester) Concurrency() int {
	return 15
}

// ParseMsg takes a JSON payload and attempts to parse it as a SiteRequest
// message. If the message doesn't validate or parse, we return an error.
// TODO fix this to be agnostic to the serialization type; we should support
// JSON, protobuf, etc.
func (SiteRequester) ParseMsg(data []byte) (workers.Message, error) {
	var msg types.SiteRequest
	err := json.Unmarshal(data, &msg)

	if err != nil {
		return nil, workers.NewParseErr(err, msg, data)
	}

	return msg, nil
}

// Handle takes a request for a site and attempts to download its contents.
// These will be sent off down the pipeline for processing.
func (sr *SiteRequester) Handle(msg workers.Message) error {
	log.SetPrefix("SiteRequester/Handle")
	req, ok := msg.(types.SiteRequest)
	if !ok {
		return workers.WrapWithRetry(
			fmt.Errorf("expected SiteRequest message, got %v", msg),
			false,
		)
	}
	log.Println("Received request to scrape site", req.URL)

	// TODO detect URL without protocol and fall back to http
	if !strings.HasPrefix(req.URL, "http") {
		req.URL = "http://" + req.URL
	}

	if err := sr.db.CreateEntry(req); err != nil {
		return workers.WrapWithRetry(err, true)
	}

	// TODO Wrap errors
	resp, err := http.Get(req.URL)
	if err != nil {
		return workers.WrapWithRetry(err, true)
	}

	switch resp.StatusCode {
	case http.StatusNotFound, http.StatusGone:
		return workers.WrapWithRetry(
			fmt.Errorf("site '%s' is not found", req.URL),
			false,
		)

	case http.StatusInternalServerError, http.StatusServiceUnavailable,
		http.StatusGatewayTimeout, http.StatusRequestTimeout,
		http.StatusTooManyRequests:
		return workers.WrapWithBackoff(
			fmt.Errorf("remote server not ready with status: %d", resp.StatusCode),
			2*time.Second,
		)
	}

	defer resp.Body.Close()
	compressed, err := compress.Compress(resp.Body)
	if err != nil {
		fmt.Printf("error compressing payload: %v\n", err)
		return workers.WrapWithRetry(err, true)
	}

	sr.Publish(types.SiteLDJSON{
		SiteID:  req.SiteID,
		URL:     req.URL,
		Payload: compressed,
	})
	sr.Publish(types.SiteMicrodata{
		SiteID:  req.SiteID,
		URL:     req.URL,
		Payload: compressed,
	})

	return nil
}

// SetPublisher receives a reference to the NSQ transport's message bus so the
// handler can send messages back to NSQ
func (sr *SiteRequester) SetPublisher(bus *workers.MessageBus) {
	sr.bus = bus
}

// Publish sends a message to the publishing bus to be forwarded on to NSQ.
// Since the scraped site contents might be rather large, we want to compress it
// before forwarding on. NSQ has a max message size (defaults to 1048576 bytes)
// that we don't want to surpass in case the site is large.
//
// The current implementation compresses *before* calling Publish. See the
// Handle method for more information.
func (sr *SiteRequester) Publish(msg workers.Message) error {
	// TODO switch all internal messaging to protobuf
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	sr.bus.Publish(workers.BusData{
		Data: data,
		Type: sr.Accepts(),
	})
	return nil
}
