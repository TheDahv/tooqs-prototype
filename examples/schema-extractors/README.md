# Schema.org Extraction Pipeline

This reference application demonstrates how to use TOOQS to build a pipeline to
extract and save [schema.org](http://schema.org/) information from a given
website.

## Background Context

A website can mark up information about itself in a semantically relevant way to
crawlers and other programmatic parsing tools. There are 2 popular formats most
sites use: 

- json+ld - a specific JSON format to describe a website and its contents,
  independent of that content
- microdata - a special subset of attributes to apply to HTML tags to describe
  the content they contain

A website can use one of these or none of these, and there isn't an obvious way
to know ahead of time without processing the page and seeing what it contains.

## Processing Pipeline

The pipeline begins with a [`SiteRequester`](./requester/worker.go) that
receives sites to inspect and kicks off the pipeline. This worker has an
*explicitly-named* topic that it listens on. Because this is a start of a
pipeline, we _want_ a human-friendly name and we _want_ for the outside world to
be able to submit work to it.

Because we don't know what kind of schema.org implementation the site will have
before we run the pipeline, we create a row for it in our database with an
implementation value of 'none' to a) indicate that we received the request for
that site, b) we don't know what implementation it has yet.

Then we forward work to 2 pipeline workers running in
parallel--[`JSONExtractor`](./extractors/json/worker.go) looks for json+ld and
[`MicrodataExtractor`](./extractors/json/worker.go) looks for microdata . Both
of these workers have topic and channel names automatically generated by
[tooqs-namer](../../tools/cmd/tooqs-namer/main.go), which indicates that they
aren't meant to be receive work from outside the pipeline.

If either worker finds schema.org information in the page, the contents are
normalized in a JSON format and the content and implementation type are
forwarded on to the last stage of the pipeline.

The [`Receiver`](./receiver/worker.go) worker takes the incoming payload with
the data encountered and saves it in the database.
