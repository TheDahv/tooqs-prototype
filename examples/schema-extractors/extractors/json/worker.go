package json

//go:generate tooqs-namer -type JSONExtractor .

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"reflect"

	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/compress"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/types"
	"gitlab.com/thedahv/tooqs-prototype/workers"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// JSONExtractor receives site payload information and extracts and LD+JSON
// information found therein.
type JSONExtractor struct {
	bus *workers.MessageBus
}

// Accepts defines the type of message JSONExtractor can process
func (JSONExtractor) Accepts() reflect.Type {
	return reflect.TypeOf(types.SiteLDJSON{})
}

// ParseMsg takes a JSON payload and attempts to parse it as a JSONExtractor
// message. If the message doesn't validate or parse, we return an error.
// TODO fix this to be agnostic to the serialization type; we should support
// JSON, protobuf, etc.
func (JSONExtractor) ParseMsg(data []byte) (workers.Message, error) {
	var msg types.SiteLDJSON
	err := json.Unmarshal(data, &msg)

	if err != nil {
		return nil, workers.NewParseErr(err, msg, data)
	}

	return msg, nil
}

// Concurrency defines how many messages we want to handle at once. The JSON
// extractor runtime is dominated by the IO wait time to fetch a web page. As a
// result, we want to at least double the concurrency with respect to the
// previous worker in the pipeline.
func (JSONExtractor) Concurrency() int {
	return 10
}

// Handle receives the contents of a web page and attempts to locate a script
// tag with the JSON+LD type attribute. If encountered, it extracts the JSON
// inside and forwards on information to the next step in the pipeline. It does
// not publish any messages if no content is found.
//
// Note, messages forwarded to this pipeline worker come compressed from the
// SiteRequester handler, so we need to decompress them here.
func (j JSONExtractor) Handle(msg workers.Message) error {
	req, ok := msg.(types.SiteLDJSON)
	if !ok {
		return workers.WrapWithRetry(
			fmt.Errorf("expected SiteRequest message, got %v", msg),
			false,
		)
	}

	payload, err := compress.Decompress(bytes.NewReader(req.Payload))
	if err != nil {
		workers.WrapWithRetry(err, true)
	}

	r := bytes.NewReader(payload)
	z := html.NewTokenizer(r)

	var entries [][]byte

	for {
		if z.Next() == html.ErrorToken {
			if z.Err() == io.EOF {
				break
			}
		}

		tt := z.Token()
		if isLdJson(tt) {
			z.Next()

			if t := z.Token(); t.Type == html.TextToken {
				src := []byte(t.Data)
				dest := make([]byte, len(src))
				copy(dest, src)
				entries = append(entries, dest)
			}
		}
	}

	// TODO handle inconsistency of type and value prefixes (namely, '@Type' vs
	// 'Type') between this and microdata extraction
	if len(entries) > 0 {
		var buf bytes.Buffer
		buf.WriteByte('[')

		for i, e := range entries {
			_, err := buf.Write(e)
			if err != nil {
				return workers.WrapWithRetry(err, true)
			}
			if i < len(entries)-1 {
				buf.WriteByte(',')
			}
		}
		buf.WriteByte(']')

		j.Publish(types.ExtractedSchema{
			SiteID:         req.SiteID,
			URL:            req.URL,
			Schema:         buf.Bytes(),
			Implementation: types.LDJson,
		})
		return nil
	}

	return nil
}

// SetPublisher receives a reference to the NSQ transport's message bus so the
// handler can send messages back to NSQ
func (j *JSONExtractor) SetPublisher(bus *workers.MessageBus) {
	j.bus = bus
}

// Publish sends a message to the publishing bus to be forwarded on to NSQ
func (j *JSONExtractor) Publish(msg workers.Message) error {
	// TODO switch all internal messaging to protobuf
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	j.bus.Publish(workers.BusData{
		Data: data,
		Type: j.Accepts(),
	})
	return nil
}

func isLdJson(t html.Token) bool {
	if t.DataAtom != atom.Script {
		return false
	}

	for _, a := range t.Attr {
		if a.Key == "type" && a.Val == "application/ld+json" {
			return true
		}
	}

	return false
}
