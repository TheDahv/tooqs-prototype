package microdata

//go:generate tooqs-namer -type MicrodataExtractor .

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
	"reflect"

	"github.com/namsral/microdata"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/compress"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/types"
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// MicrodataExtractor receives site payload information and extracts and
// schema.org content encoded in microdata tags.
type MicrodataExtractor struct {
	bus *workers.MessageBus
}

// Accepts defines what kind of message the MicrodataExtractor accepts
func (MicrodataExtractor) Accepts() reflect.Type {
	return reflect.TypeOf(types.SiteMicrodata{})
}

// Concurrency defines how many messages we want to handle at once. The
// Microdata extractor runtime is dominated by the IO wait time to fetch a web
// page. As a result, we want to at least double the concurrency with respect to
// the previous worker in the pipeline.
func (MicrodataExtractor) Concurrency() int {
	return 10
}

// ParseMsg takes a JSON payload and attempts to parse it as a JSONExtractor
// message. If the message doesn't validate or parse, we return an error.
// TODO fix this to be agnostic to the serialization type; we should support
// JSON, protobuf, etc.
func (MicrodataExtractor) ParseMsg(data []byte) (workers.Message, error) {
	var msg types.SiteMicrodata
	err := json.Unmarshal(data, &msg)

	if err != nil {
		return nil, workers.NewParseErr(err, msg, data)
	}

	return msg, nil
}

// Handle receives the contents of a web page and attempts to find any content
// marked up in microdata. Unlike JSON+LD where all schema.org content is
// confined to one tag, this information is spread across the page. Therefore,
// we have to walk the entire DOM tree to find the information we want.
//
// Note, messages forwarded to this pipeline worker come compressed from the
// SiteRequester handler, so we need to decompress them here.
func (m MicrodataExtractor) Handle(msg workers.Message) error {
	req, ok := msg.(types.SiteMicrodata)
	if !ok {
		return workers.WrapWithRetry(
			fmt.Errorf("expected SiteRequest message, got %v", msg),
			false,
		)
	}

	u, err := url.Parse(req.URL)
	if err != nil {
		return workers.WrapWithRetry(err, false)
	}

	payload, err := compress.Decompress(bytes.NewReader(req.Payload))
	if err != nil {
		workers.WrapWithRetry(err, true)
	}

	md, err := microdata.ParseHTML(bytes.NewReader(payload), "", u)
	if err != nil {
		return workers.WrapWithRetry(err, true)
	}

	if len(md.Items) == 0 {
		return nil
	}

	// TODO handle inconsistency of type and value prefixes (namely, '@Type' vs
	// 'Type') between this and JSON+LD extraction
	d, err := json.Marshal(md.Items)
	if err != nil {
		return workers.WrapWithRetry(err, true)
	}

	m.Publish(types.ExtractedSchema{
		SiteID:         req.SiteID,
		URL:            req.URL,
		Schema:         d,
		Implementation: types.Microdata,
	})

	return nil
}

// SetPublisher receives a reference to the NSQ transport's message bus so the
// handler can send messages back to NSQ
func (m *MicrodataExtractor) SetPublisher(bus *workers.MessageBus) {
	m.bus = bus
}

// Publish sends a message to the publishing bus to be forwarded on to NSQ
func (m *MicrodataExtractor) Publish(msg workers.Message) error {
	// TODO switch all internal messaging to protobuf
	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	m.bus.Publish(workers.BusData{
		Data: data,
		Type: m.Accepts(),
	})
	return nil
}
