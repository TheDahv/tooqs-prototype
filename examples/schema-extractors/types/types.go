package types

//go:generate stringer -type SchemaImplementation

// SiteRequest is a request to scrape a site for its ld+json data or its
// microdata, which ever it finds.
type SiteRequest struct {
	SiteID string `json:"siteId"`
	URL    string `json:"url"`
}

// Msg implements the handlers.Message interface
func (sr SiteRequest) Msg() {}

// SitePayload represents the common interface for working with extracted site
// data.
// type SitePayload interface {
// 	Data() []byte
// }

// SiteLDJSON holds the extracted ld+json encountered in a site. The
// payload will be empty if none was found.
type SiteLDJSON struct {
	SiteID  string `json:"siteId"`
	URL     string `json:"url"`
	Payload []byte `json:"payload"`
}

// Msg implements the handlers.Message interface
func (ldj SiteLDJSON) Msg() {}

// Data returns the ld+json payload extracted from the site and implements the
// SitePayload interface.
func (ldj SiteLDJSON) Data() []byte {
	return ldj.Payload
}

// SiteMicrodata holds the extracted microdata encountered in a site. The payload
// will be empty if none was found.
type SiteMicrodata struct {
	SiteID  string `json:"siteId"`
	URL     string `json:"url"`
	Payload []byte `json:"payload"`
}

// Msg implements the handlers.Message interface
func (m SiteMicrodata) Msg() {}

// Data returns the ld+json payload extracted from the site and implements the
// SitePayload interface.
func (m SiteMicrodata) Data() []byte {
	return m.Payload
}

// SchemaImplementation indicates how a site implements schema.org. Because it's
// a value that we want stored in a database in a readable way, we use the
// 'stringer' tool to turn our enum into strings.
//
// ./types/schemaimplementation_string.go is a generated file, so don't touch
// it. Run `stringer -type=SchemaImplementation` in the `./types` folder if we
// ever update this enum.
type SchemaImplementation int

const (
	// LDJson indicates a site that implemented schema.org with ld+json
	LDJson SchemaImplementation = iota
	// Microdata indicates a site taht implemented schema.org with microdata
	Microdata
	// None indicates a site that has no schema.org markup
	None
)

// ExtractedSchema holds information about schema.org extracted from a page,
// what the contents of the extraction are, and what implementation the site
// used to encode the structured data.
type ExtractedSchema struct {
	SiteID         string               `json:"siteId"`
	URL            string               `json:"url"`
	Schema         []byte               `json:"schema"`
	Implementation SchemaImplementation `json:"type"`
}

// Msg implements the handlers.Message interface
func (e ExtractedSchema) Msg() {}
