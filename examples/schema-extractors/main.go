package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitlab.com/thedahv/tooqs-prototype/driver"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/db"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/extractors/json"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/extractors/microdata"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/receiver"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/requester"
	"gitlab.com/thedahv/tooqs-prototype/transport/nsq"
)

func main() {
	var nsqdHost = flag.String(
		"nsqd-host",
		"localhost",
		"nsqd host",
	)
	var nsqdPort = flag.Int(
		"nsqd-port",
		4150,
		"nsqd port",
	)
	var nsqlookupds = flag.String(
		"nsqlookupds",
		"localhost:4160,localhost:4161",
		"nsqlookupd addresses (comma-separated)",
	)

	var pgHost = flag.String(
		"pg-host",
		"localhost",
		"postgres host",
	)
	var pgUser = flag.String(
		"pg-user",
		"schema_extractor",
		"the database user for the connection",
	)
	var pgPass = flag.String(
		"ps-pass",
		"schema_extractor",
		"the database user password for the connection",
	)

	flag.Parse()

	connURL := fmt.Sprintf("user=%s password=%s host=%s dbname=%s sslmode=disable",
		*pgUser, *pgPass, *pgHost, "schema_extractor")
	fmt.Println("Connecting to PG at", connURL)
	client, err := db.New(connURL)
	if err != nil {
		log.Fatalf("Unable to connect to database: %s\n", err.Error())
	}

	if err := client.Ping(); err != nil {
		log.Fatalf("Database connection issue: %s\n", err.Error())
	}

	err = client.Prepare()
	if err != nil {
		log.Fatalf("Problem setting up database: %s\n", err.Error())
	}

	fmt.Println("Client created", client == nil)

	n, err := nsq.New(*nsqdHost, *nsqdPort, strings.Split(*nsqlookupds, ",")...)
	if err != nil {
		fmt.Printf("Error creating NSQ transport: %v\n", err)
	}

	d := &driver.Driver{}

	err = d.Register(
		n,
		requester.New(client),
		&json.JSONExtractor{},
		&microdata.MicrodataExtractor{},
		receiver.New(client),
	)
	if err != nil {
		fmt.Printf("Error registering pipeline: %v\n", err)
		os.Exit(2)
	}

	fmt.Println("Workers registered and listening")
	fmt.Println(d)

	shutdown := make(chan os.Signal, 2)
	signal.Notify(shutdown, syscall.SIGINT)

	// TODO use select and wait on channels to listen for quits, config changes,
	// notifications, etc
	// Also, we can pass a quit channel into our drivers to pass things around
	select {
	case <-shutdown:
		client.Quit()
		d.StopAll()
		os.Exit(0)
	}
}
