package db

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/types"
	"gitlab.com/thedahv/tooqs-prototype/workers"

	_ "github.com/lib/pq"
)

// Client manages communication with the underlying database and provides
// methods to extract and add data to it
type Client struct {
	conn *sql.DB
}

// New attempts to connect to the database at `url` and returns a connected
// client. This argument is expected to be a valid PG connection URL following
// the format:
//		postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full
func New(url string) (*Client, error) {
	c, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}
	return &Client{conn: c}, nil
}

// Quit closes the underlying connection and cleans up any other resources
func (c *Client) Quit() error {
	return c.conn.Close()
}

// Ping verifies a connection to the database
func (c *Client) Ping() error {
	return c.conn.Ping()
}

// Prepare handles setting up the database and any tables if they don't already
// exist.
func (c *Client) Prepare() error {
	log.SetPrefix("[db/Prepare] ")
	q := `CREATE TABLE IF NOT EXISTS scrapes (
		site_id UUID PRIMARY KEY,
		site_url VARCHAR(512) NOT NULL,
		implementation VARCHAR(32) NOT NULL DEFAULT 'none',
		payload JSON
	);`

	log.Println("Creating table")
	r, err := c.conn.Exec(q)
	if err != nil {
		return fmt.Errorf("error setting up results table: %s", err.Error())
	}
	num, _ := r.RowsAffected()
	log.Println("No table error. Moving on", num)

	return nil
}

// CreateEntry adds an entry for a site we are going to scrape. This records the
// beginning of a pipeline run.
func (c *Client) CreateEntry(req types.SiteRequest) error {
	log.SetPrefix("[db/CreateEntry] ")
	q := `INSERT INTO scrapes
	(site_id, site_url)
	VALUES
	($1, $2)
	ON CONFLICT (site_id) DO UPDATE
		SET site_url = EXCLUDED.site_url,
				implementation = 'none',
				payload = NULL`

	log.Printf("Initializing pipeline state for %s at %s\n",
		req.SiteID, req.URL)
	_, err := c.conn.Exec(q, req.SiteID, req.URL)
	if err != nil {
		return err
	}

	return nil
}

// AddPayloadToEntry updates the entry with the result of running through the
// pipeline.
func (c *Client) AddPayloadToEntry(req types.ExtractedSchema) error {
	log.SetPrefix("[db/AddPayloadToEntry] ")
	q := `UPDATE scrapes
	SET
		implementation = $1,
		payload = $2
	WHERE
		site_id = $3`

	log.Printf("Adding extracted %s payload to %s\n", req.Implementation, req.SiteID)
	r, err := c.conn.Exec(
		q,
		req.Implementation.String(),
		req.Schema,
		req.SiteID,
	)

	if err != nil {
		workers.WrapWithBackoff(
			fmt.Errorf("error updating scrape entry: %s", err.Error()),
			2*time.Second,
		)
	}

	if r == nil {
		log.Println("Expected to see a results object. Error is:", err)
		return workers.WrapWithRetry(
			fmt.Errorf("expected a results object, got none"),
			true,
		)
	}

	// NOTE dropping update row checking on the floor for now
	if n, err := r.RowsAffected(); err == nil {
		if n != 1 {
			return workers.WrapWithBackoff(
				fmt.Errorf("updated %d entries, expected 1", n),
				5*time.Second,
			)
		}
	} else {
		log.Printf("row checking error: %s\n", err.Error())
	}

	return nil
}
