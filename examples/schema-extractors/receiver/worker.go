package receiver

//go:generate tooqs-namer -type Receiver .

import (
	"encoding/json"
	"fmt"
	"reflect"

	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/db"
	"gitlab.com/thedahv/tooqs-prototype/examples/schema-extractors/types"
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// Receiver receives messages containing the extracted schema.org information in
// a given site and saves it.
type Receiver struct {
	db *db.Client
}

// New creates a new Receiver worker with the configured db client
func New(db *db.Client) *Receiver {
	return &Receiver{db: db}
}

// Accepts defines the type of message this worker can process
func (Receiver) Accepts() reflect.Type {
	return reflect.TypeOf(types.ExtractedSchema{})
}

// Concurrency sets how many messages Receiver should process at once
func (Receiver) Concurrency() int {
	return 5
}

// ParseMsg takes a JSON payload and attempts to parse it as an ExtractedSchema
// message. If the message doesn't validate or parse, we return an error.
func (Receiver) ParseMsg(data []byte) (workers.Message, error) {
	var msg types.ExtractedSchema
	err := json.Unmarshal(data, &msg)

	if err != nil {
		return nil, workers.NewParseErr(err, msg, data)
	}

	return msg, nil
}

// Handle receives messages about a scraped Schema.org payload and saves them to
// the database. It does not publish any further messages.
func (r Receiver) Handle(msg workers.Message) error {
	req, ok := msg.(types.ExtractedSchema)
	if !ok {
		return workers.WrapWithRetry(
			fmt.Errorf("expected ExtractedSchema message, got %v", msg),
			false,
		)
	}

	if err := r.db.AddPayloadToEntry(req); err != nil {
		return workers.WrapWithRetry(err, true)
	}

	return nil
}
