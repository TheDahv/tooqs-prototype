package workers

import (
	"fmt"
	"reflect"
	"time"
)

// Message is the common interface for all message types. Each message should be
// parseable from the outside world to one of these types, and should support
// validating and posting a message to its expected topic from anywhere in the
// system.
type Message interface {
	// An empty method that merely identifies a type as implementing the Message
	// interface.
	Msg()
}

// MessageBus is used by types implementing Publisher to submit work to the
// transport. The transport is responsible for giving the Publisher a working
// message bus via `func (Publisher) SetPublisher`. The Handler is responsible
// for calling it when there is data to add.
type MessageBus struct {
	bus chan BusData
}

// BusData defines data that can go across the MessageBus. Its data type is just
// a stream of bytes which implies Publisher and Handler implementation can
// define their own encoding strategy.
type BusData struct {
	Data []byte // TODO consider usefulness of io.Reader instead
	Type reflect.Type
}

// NewMessageBus creates and returns a new bus
func NewMessageBus() *MessageBus {
	return &MessageBus{
		bus: make(chan BusData),
	}
}

// Publish adds a message to the message bus. It will panic if the bus has not
// been initialized via NewMessageBus
func (mb *MessageBus) Publish(data BusData) {
	mb.bus <- data
}

// Messages returns the stream of messages carried over the internal channel
func (mb *MessageBus) Messages() chan BusData {
	return mb.bus
}

// Quit tears down the message bus
func (mb *MessageBus) Quit() {
	close(mb.bus)
}

// Worker describes all the behavior required of a type that can receive
// messages from the transport, process them and possibly fail, and potentially
// publish new messages back to the transport.
/*
type Worker interface {
	Handler
	Publisher
}
*/

// Handler describes behavior common across all types that can receive messages
// from a transport for their registered Message type. The transport will call
// `func (Handler) Handle(Message) error` once for every message on the incoming
// stream.
type Handler interface {
	// Accepts defines what kind of message a handler knows how to process. The
	// system uses this information to look up Handlers that should receive a new
	// message, and to validate messages passed to a handler are of the correct
	// type.
	Accepts() reflect.Type
	// ParseMsg is the counterpart of Accepts and parses raw data encoding a
	// message (possibly in JSON, etc) into a typed message struct or an error if
	// parsing failed.
	ParseMsg([]byte) (Message, error)
	// Topic names the "subject" or "event" stream that the handler processes
	Topic() string
	// Channel names the specific action that the Handler will perform on messages
	// in the Topic stream. It distinguishes it from other Handlers that may
	// operate on the same channel
	Channel() string
	// Channel defines the number of instances of the handler that the transport
	// should use to process incoming work
	Concurrency() int
	// Handle actually performs the work of receiving a typed message struct and
	// processing it. It can return an error to indicate the processing failed.
	// See the various Error interfaces defined in this package (`Retriable`,
	// `Backoffable`) to see how the transport should respond to your error.
	Handle(Message) error
}

// Publisher describes types that can publish messages to the transport
type Publisher interface {
	SetPublisher(*MessageBus)
	Publish(Message) error
}

// Retriable is a handler error that should or should not be retried.
type Retriable interface {
	error
	Retry() bool
}

// retrier wraps an error and the Retry condition for that error
type retrier struct {
	error
	retry bool
}

// WrapWithRetry wraps an error with its retriable status
func WrapWithRetry(err error, retry bool) Retriable {
	return retrier{err, retry}
}

// Retry returns whether the transport should retry a message that errored
func (r retrier) Retry() bool {
	return r.retry
}

// Backoff is a handler error that specifies how many milliseconds the transport
// should wait before re-enqeueing an errored message.
type Backoffable interface {
	error
	Backoff() time.Duration
}

// backoffer wraps an error and the Backoff value
type backoffer struct {
	error
	backoff time.Duration
}

// WrapWithBackoff wraps an error with its backfoff value
func WrapWithBackoff(err error, backoff time.Duration) Backoffable {
	return backoffer{err, backoff}
}

// Backoff returns the wait value in milliseconds before reqeueuing a failed
// message
func (b backoffer) Backoff() time.Duration {
	return b.backoff
}

// ParseErr represents a problem converting a transport-level payload into a
// type expected for a given worker.
type ParseErr interface {
	Error() string
	Payload() []byte
	Retry() bool
}

type parseerr struct {
	Err     error
	Msg     Message
	Payload []byte
}

// NewParseErr returns an error representing a failed parse
func NewParseErr(err error, m Message, data []byte) error {
	return parseerr{
		Err:     err,
		Msg:     m,
		Payload: data,
	}
}

// Implement retriable for ParseErr. ParseErr errors are never retriable.
func (parseerr) Retry() bool {
	return false
}

// Error implements the error interface for parserr.
func (pe parseerr) Error() string {
	// TODO use errors.Wrap
	return fmt.Sprintf("error parsing message payload into expected type: %t",
		pe.Msg)
}
