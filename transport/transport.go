package transport

import (
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// Transporter negotiates communication with the real-world queueing system. It
// normalizes over any operation a client may want without leaking any
// service-specific semantics.
type Transporter interface {
	// Connect establishes a relationship with the real-world queueing system.
	Connect() error
	// AddHandler receives a handler that receives messages and processes them.
	// If the handler returns a message-level error, the Transporter handles
	// messaging back to the queueing system dependent on the behavior defined by
	// that error. If the handler also implements Publisher behavior, it must also
	// set the message bus for worker.
	AddHandler(workers.Handler) error
	// Stop should spin down all workers and disconnect from the queueing system.
	Stop()
}
