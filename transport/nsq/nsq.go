package nsq

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"reflect"
	"sync"
	"time"

	nsq "github.com/nsqio/go-nsq"
	"gitlab.com/thedahv/tooqs-prototype/workers"
)

// NSQ is the interface to register workers with NSQ. It connects to at least
// one nsqlookupd to find topics that emit messages, look up workers based on
// their declared listening topic, and parses NSQ messages into Worker messages.
//
// It also provides a message bus for workers to publish messages into and finds
// which topics to post to based on the incoming message type of the registered
// worker.
type NSQ struct {
	Host           string
	Port           int
	NSQProducer    *nsq.Producer
	NSQLookupds    []string
	config         *nsq.Config
	bus            *workers.MessageBus
	entries        []*Entry
	entryLock      sync.RWMutex
	defaultBackoff time.Duration
}

// Entry represents a worker connected to this transport. We use it to keep
// track of workers for message dispatch and cleanup.
type Entry struct {
	Accepts     reflect.Type
	Channel     string
	NSQConsumer *nsq.Consumer
	Topic       string
	Worker      workers.Handler
}

// New Returns a configured NSQTransport
func New(host string, port int, lookupds ...string) (*NSQ, error) {
	n := &NSQ{
		Host:           host,
		Port:           port,
		config:         nsq.NewConfig(),
		bus:            workers.NewMessageBus(),
		NSQLookupds:    lookupds,
		defaultBackoff: 100 * time.Millisecond,
	}

	// TODO Move this out of initializer
	if err := n.Listen(); err != nil {
		return nil, err
	}

	return n, nil
}

// AddEntry adds a new NSQ handlers to the map of message types to workers.
func (n *NSQ) AddEntry(e *Entry) {
	n.entryLock.Lock()
	defer n.entryLock.Unlock()

	n.entries = append(n.entries, e)
}

// Connect doesn't need to do anything for NSQ since the connections happen when
// we set up the Consumers and Producers.
// TODO deprecate if it't not part of the final API
func (n *NSQ) Connect() error {
	return nil
}

// AddHandler registers a handler with NSQ to consume messages from a specific
// topic on its own channel. Each handler is connected via a [NSQ
// Consumer](https://godoc.org/github.com/nsqio/go-nsq#Consumer) at the
// concurrency level specified by the Handler (note, the NSQ client turns this
// concurrency value into the number of goroutines used to process work).
//
// If the handler also implements the Publisher interface, it also connects it
// to the message bus.
func (n *NSQ) AddHandler(h workers.Handler) error {
	if p, ok := h.(workers.Publisher); ok {
		log.SetPrefix("[Transport/AddROWorker] ")

		if n.bus == nil {
			return fmt.Errorf("attempted to add uninitialized publisher to worker")
		}

		// Give the workers a way to emit new messages.
		p.SetPublisher(n.bus)
	}

	topic := h.Topic()
	if topic == "" {
		topic = randomName(20)
	}
	channel := h.Channel()
	if channel == "" {
		channel = randomName(20)
	}

	// The Entry is our way of keeping track of workers connected on this
	// transport and what kinds of messages should be dispatched to them. We'll
	// connect and configure the handler and then add the entry to our internal
	// list if nothing goes wrong.
	e := &Entry{
		Accepts: h.Accepts(),
		Worker:  h,
		Topic:   topic,
		Channel: channel,
	}

	log.Printf("Adding handler for %s messages at %s/%s... ",
		h.Accepts().String(), topic, channel)
	if c, err := nsq.NewConsumer(topic, channel, n.config); err == nil {
		log.Println("Consumer created!")
		e.NSQConsumer = c
	} else {
		log.Println("Consumer creation error!")
		log.Println(err)
		return err
	}

	// This sets up the bridge between NSQ and the TOOQS worker. We set up a NSQ
	// client that forwards the message to the worker, receives a result, and
	// signals the status back to NSQ.
	e.NSQConsumer.AddConcurrentHandlers(nsq.HandlerFunc(func(m *nsq.Message) error {
		log.SetPrefix("[Transport/AddWorker(handler)] ")
		msg, err := h.ParseMsg(m.Body)

		// Swallow/log parse errs and finish the message so we don't try it again
		if err != nil {
			// TODO log and track parse error
			log.Printf("Received bad message for worker: %s\n", err)
			m.Finish()
			return nil
		}

		if err := h.Handle(msg); err != nil {
			log.Printf("Error processing message: %s\n", err)

			switch err := err.(type) {
			case workers.Retriable:
				if err.Retry() {
					m.RequeueWithoutBackoff(n.defaultBackoff)
				} else {
					m.Finish()
				}
				return nil

			case workers.Backoffable:
				m.Requeue(err.Backoff())
				return nil
			}

			return err
		}

		m.Finish()
		return nil
	}), h.Concurrency())
	// TODO Introduce the concept of "pipeline scale" so that "worker scale" can
	// be relative to each other. That way, saying a worker runs "2X with respect
	// to base", you can scale the entire pipeline together and still respect the
	// relative scale parity relationships within the worker

	// TODO explore how handler concurrency relates to max_in_flight. Having
	// multiple goroutines to process more work but not sending more work seems
	// counter-intuitive
	//e.Consumer.ChangeMaxInFlight(h.Concurrency())

	log.Println("Connecting to nsqlookupd")
	e.NSQConsumer.ConnectToNSQLookupds(n.NSQLookupds)

	log.Println("Creating topic and channel in NSQD", topic, channel)
	if err := n.RegisterTopic(topic, channel); err != nil {
		log.Println("Topic create error", err.Error())
		return err
	}

	n.AddEntry(e)

	return nil
}

// RegisterTopic pre-creates a topic and channel before we publish to it. go-nsq
// seems to not expose a method to do this (that, or we can't seem make a real
// nsq.Conn on which we could send the nsq.Command) so we're falling back to
// using the HTTP endpoint for this
func (n *NSQ) RegisterTopic(topic, channel string) error {
	log.SetPrefix("[Transport/RegisterTopic] ")

	// TODO well I guess we need the HTTP port too
	addr := fmt.Sprintf("http://%s:%d", n.Host, 4151)
	ops := []struct {
		name string
		addr string
	}{
		{
			fmt.Sprintf("topic '%s'", topic),
			fmt.Sprintf("%s/topic/create?topic=%s", addr, topic),
		},
		{
			fmt.Sprintf("channel: '%s'", channel),
			fmt.Sprintf("%s/channel/create?topic=%s&channel=%s", addr, topic, channel),
		},
	}

	c := http.DefaultClient
	for _, op := range ops {
		log.Printf("Creating %s with NSQD at %s\n", op.name, addr)
		req, err := http.NewRequest("POST", op.addr, nil)
		if err != nil {
			return err
		}
		resp, err := c.Do(req)
		if err != nil {
			return err
		}

		if resp.StatusCode != http.StatusOK {
			return fmt.Errorf("%s create request returned status %d",
				op.name, resp.StatusCode)
		}
	}

	log.Println("Done setting up topic and channel")
	return nil
}

// Listen receives messages Published by worker and dispatches them to the
// workers expecting those messages. Since we publish directly to a single NSQd
// (and not to many nsqlookupds), this only needs to be called once per
// Transport.
func (n *NSQ) Listen() error {
	log.SetPrefix("[Transport/Listen ]")

	// Bail out if we've done this already
	if n.NSQProducer != nil {
		return nil
	}

	log.Println("Configuring producer for", fmt.Sprintf("%s:%d", n.Host, n.Port))
	p, err := nsq.NewProducer(
		fmt.Sprintf("%s:%d", n.Host, n.Port),
		n.config,
	)

	if err != nil {
		return err
	}

	n.NSQProducer = p

	// Verify the connection. Bail hard here since we don't return errors
	if err := n.NSQProducer.Ping(); err != nil {
		log.Fatalf("Unable to connect Producer to NSQD: %s\n", err)
	}

	go func() {
		// Listen for incoming messages from workers on the shared channel. We then
		// forward that on to any NSQ topic with a worker registred to listen for
		// messages of the given type.  From there, it will make its way back to the
		// Consumer to be dispatched to the matching worker.
		for msg := range n.bus.Messages() {
			log.Println("Received message. Finding receivers for type", msg.Type)
			for _, e := range n.entries {
				if msg.Type == e.Accepts {
					log.Println("Publishing message to", e.Topic)
					if err := n.NSQProducer.Publish(e.Topic, msg.Data); err != nil {
						log.Fatalf("Unable to publish message: %s\n", err)
					}
				}
			}
		}
	}()

	return nil
}

// Stop disconnects from NSQ and tears down workers.
func (n *NSQ) Stop() {
	for _, e := range n.entries {
		e.NSQConsumer.Stop()
	}
	n.NSQProducer.Stop()
	n.bus.Quit()
}

// TODO Replace with something nicer
// https://www.calhoun.io/creating-random-strings-in-go/
var seed = rand.New(rand.NewSource(time.Now().UnixNano()))

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func randomName(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seed.Intn(len(charset))]
	}
	return string(b)
}
